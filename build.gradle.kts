val homeDir = System.getenv("HOME") ?: ""

plugins {
    kotlin("multiplatform") version "1.8.0"
}

group = "org.example"
version = "0.1"

repositories {
    mavenCentral()
}

kotlin {
    val lvDriversVer = "8.0"
    val lvglVer = "8.0.2"
    val lvglDir = "$homeDir/c_libs/lvgl/lvgl-$lvglVer"
    val lvDriversBaseDir = "$homeDir/c_libs/lv_drivers"
    val lvDriversCompilerOpts = arrayOf(
        "-DLV_DRV_NO_CONF",
        "-DUSE_MONITOR=1",
        "-DUSE_KEYBOARD=1",
        "-DUSE_MOUSE=1",
        "-DUSE_MOUSEWHEEL=1",
        "-DUSE_FBDEV=0",
        "-DUSE_EVDEV=1",
        "-D_REENTRANT",
        // Prevent SDL's "undefined reference to WinMain" issue occurring.
        "-DSDL_MAIN_HANDLED"
    )
    val lvglCompilerOpts = arrayOf(
        "-DLV_CONF_SKIP",
        "-DLV_COLOR_DEPTH=32",
    )

    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("lvgl") {
                includeDirs(
                    lvglDir,
                    "$lvDriversBaseDir/lv_drivers-$lvDriversVer/display",
                    "$lvDriversBaseDir/lv_drivers-$lvDriversVer/indev",
                    "$lvDriversBaseDir/lv_drivers-$lvDriversVer/sdl",
                    lvDriversBaseDir,
                    "/usr/include",
                    "/usr/include/x86_64-linux-gnu",
                    "/usr/include/SDL2"
                )
                compilerOpts(*lvglCompilerOpts, *lvDriversCompilerOpts)
            }
        }
        binaries {
            executable("lvgl_test") {
                entryPoint = "org.example.lvglTest.main"
            }
        }
    }
}
