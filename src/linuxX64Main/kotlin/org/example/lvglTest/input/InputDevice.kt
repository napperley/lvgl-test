package org.example.lvglTest.input

import kotlinx.cinterop.CPointer
import lvgl.lv_indev_t

internal class InputDevice private constructor(ptr: CPointer<lv_indev_t>?) {
    val lvIndevPtr: CPointer<lv_indev_t>? = ptr

    companion object {
        fun fromPointer(ptr: CPointer<lv_indev_t>?): InputDevice = InputDevice(ptr)
    }
}
