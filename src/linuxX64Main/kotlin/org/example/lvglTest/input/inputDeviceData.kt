package org.example.lvglTest.input

import kotlinx.cinterop.CPointer
import lvgl.lv_indev_data_t

internal class InputDeviceData private constructor(ptr: CPointer<lv_indev_data_t>?) {
    val lvIndevDataPtr: CPointer<lv_indev_data_t>? = ptr

    companion object {
        fun fromPointer(ptr: CPointer<lv_indev_data_t>?): InputDeviceData = InputDeviceData(ptr)
    }
}

internal fun CPointer<lv_indev_data_t>?.toInputDeviceData(): InputDeviceData = InputDeviceData.fromPointer(this)
