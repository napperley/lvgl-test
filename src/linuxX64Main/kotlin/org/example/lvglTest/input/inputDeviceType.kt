package org.example.lvglTest.input

import lvgl.lv_indev_type_t

internal enum class InputDeviceType {
    /** Uninitialized state. */
    NONE,
    /** Touchpad, mouse, external button. */
    POINTER,
    /** Keypad or keyboard. */
    KEYPAD,
    /** External (hardware button) which is assigned to a specific point of the screen. */
    BUTTON,
    /** Encoder with only Left, Right turn and a Button. */
    ENCODER
}

internal fun lv_indev_type_t.toInputDeviceType(): InputDeviceType = when (this) {
    lv_indev_type_t.LV_INDEV_TYPE_KEYPAD -> InputDeviceType.KEYPAD
    lv_indev_type_t.LV_INDEV_TYPE_NONE -> InputDeviceType.NONE
    lv_indev_type_t.LV_INDEV_TYPE_POINTER -> InputDeviceType.POINTER
    lv_indev_type_t.LV_INDEV_TYPE_ENCODER -> InputDeviceType.ENCODER
    lv_indev_type_t.LV_INDEV_TYPE_BUTTON -> InputDeviceType.BUTTON
    else -> throw IllegalStateException("Unrecognised value.")
}

internal fun InputDeviceType.toLvIndevType(): lv_indev_type_t = when (this) {
    InputDeviceType.POINTER -> lv_indev_type_t.LV_INDEV_TYPE_POINTER
    InputDeviceType.ENCODER -> lv_indev_type_t.LV_INDEV_TYPE_ENCODER
    InputDeviceType.KEYPAD -> lv_indev_type_t.LV_INDEV_TYPE_KEYPAD
    InputDeviceType.NONE -> lv_indev_type_t.LV_INDEV_TYPE_NONE
    InputDeviceType.BUTTON -> lv_indev_type_t.LV_INDEV_TYPE_BUTTON
}
