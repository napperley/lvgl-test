package org.example.lvglTest.input

import kotlinx.cinterop.*
import lvgl.lv_indev_drv_init
import lvgl.lv_indev_drv_register
import lvgl.lv_indev_drv_t
import lvgl.lv_indev_type_t
import org.example.lvglTest.Closable

internal class InputDeviceDriver private constructor(ptr: CPointer<lv_indev_drv_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    private val stableRef = if (ptr == null) StableRef.create(this) else null
    val lvIndevDrvPtr: CPointer<lv_indev_drv_t>? = ptr ?: createLvIndevDrvPtr()
    var type: InputDeviceType
        get() = (lvIndevDrvPtr?.pointed?.type ?: lv_indev_type_t.LV_INDEV_TYPE_NONE).toInputDeviceType()
        set(value) {
            lvIndevDrvPtr?.pointed?.type = value.toLvIndevType()
        }
    var onRead: (driver: InputDeviceDriver, data: InputDeviceData) -> Unit = { _, _ -> }

    private fun createLvIndevDrvPtr(): CPointer<lv_indev_drv_t>? {
        val result = arena?.alloc<lv_indev_drv_t>()?.ptr
        lv_indev_drv_init(result)
        return result
    }

    private fun setupCallbacks() {
        lvIndevDrvPtr?.pointed?.user_data = stableRef?.asCPointer()
        lvIndevDrvPtr?.pointed?.read_cb = staticCFunction { driver, data ->
            val tmp = driver?.pointed?.user_data?.asStableRef<InputDeviceDriver>()?.get()
            tmp?.onRead?.invoke(driver.toInputDeviceDriver(), data.toInputDeviceData())
        }
    }

    companion object {
        fun create(): InputDeviceDriver {
            val result = InputDeviceDriver()
            result.setupCallbacks()
            return result
        }

        fun fromPointer(ptr: CPointer<lv_indev_drv_t>?): InputDeviceDriver = InputDeviceDriver(ptr)
    }

    fun register(): InputDevice = InputDevice.fromPointer(lv_indev_drv_register(lvIndevDrvPtr))

    override fun close() {
        arena?.clear()
        stableRef?.dispose()
    }
}

internal fun CPointer<lv_indev_drv_t>?.toInputDeviceDriver(): InputDeviceDriver = InputDeviceDriver.fromPointer(this)
