package org.example.lvglTest

import kotlinx.cinterop.staticCFunction
import lvgl.*
import org.example.lvglTest.display.*
import org.example.lvglTest.display.Display
import org.example.lvglTest.display.DisplayDrawBuffer
import org.example.lvglTest.display.DisplayDriver
import org.example.lvglTest.display.register

internal object Sdl2 {
    @Suppress("UNREACHABLE_CODE")
    private fun createTickThread() {
        SDL_CreateThread(fn = staticCFunction { _ ->
            while (true) {
                SDL_Delay(5)
                // Let LVGL know that 5 ms has elapsed.
                lv_tick_inc(5)
            }
            0
        }, name = "tick", data = null)
    }

    fun initHal(enableDblBuffer: Boolean): Triple<DisplayDrawBuffer, DisplayDriver, Display> {
        monitor_init()
        // Creates a new thread that periodically informs LVGL about how much time has elapsed.
        createTickThread()
        val (displayBuffer, displayDriver, display) = createDisplay(enableDblBuffer)

        val group = lv_group_create()
        lv_group_set_default(group)
        setupTouchScreen().first.onRead = { driver, data ->
            mouse_read(driver.lvIndevDrvPtr, data.lvIndevDataPtr)
        }

        val (_, keyboardDev) = setupKeyboard()
        lv_indev_set_group(keyboardDev.lvIndevPtr, group)
        val (_, mouseWheelDev) = setupMouseWheel()
        lv_indev_set_group(mouseWheelDev.lvIndevPtr, group)
        return Triple(displayBuffer, displayDriver, display)
    }

    fun createDisplay(enableDblBuffer: Boolean): Triple<DisplayDrawBuffer, DisplayDriver, Display> {
        var display: Display? = null
        // A small buffer for LVGL to draw the screen's content.
        val drawBuf = DisplayDrawBuffer.create(MONITOR_HOR_RES.toUInt() * 10u, enableDblBuffer)
        val driver = createDisplayDriver(drawBuf) {
            horRes = MONITOR_HOR_RES.toShort()
            vertRes = MONITOR_VER_RES.toShort()
            display = register()
            if (display?.lvDispPtr == null) throw IllegalStateException("Cannot register display")
        }
        return Triple(drawBuf, driver, display!!)
    }

    fun createDisplayDriver(
        drawBuf: DisplayDrawBuffer,
        init: DisplayDriver.() -> Unit
    ): DisplayDriver {
        val result = DisplayDriver.create(drawBuf = drawBuf)
        result.onFlush = { driver, area, buf -> flush(displayDriver = driver, area = area, color = buf) }
        result.init()
        return result
    }

    fun flush(
        displayDriver: DisplayDriver,
        area: Area,
        color: Color
    ) {
        displayDriver.flush(area, color)
        lv_disp_flush_ready(displayDriver.lvDispDrvPtr)
    }
}
