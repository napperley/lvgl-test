package org.example.lvglTest

import kotlinx.cinterop.*
import lvgl.lv_color_t
import lvgl.lvglkt_color_make
import lvgl.lvglkt_color_t

internal class Color private constructor(ptr: CPointer<lv_color_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    val lvColorPtr: CPointer<lv_color_t>? = arena?.alloc<lv_color_t>()?.ptr ?: ptr
    var full: UInt
        get() = lvColorPtr?.pointed?.full ?: 0u
        set(value) {
            lvColorPtr?.pointed?.full = value
        }
    var red: UByte
        get() = lvColorPtr?.pointed?.ch?.red ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.red = value
        }
    var green: UByte
        get() = lvColorPtr?.pointed?.ch?.green ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.green = value
        }
    var blue: UByte
        get() = lvColorPtr?.pointed?.ch?.blue ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.blue = value
        }

    companion object {
        fun fromPointer(ptr: CPointer<lv_color_t>?): Color = Color(ptr)

        fun create(): Color = Color()
    }

    override fun close() {
        arena?.clear()
    }
}

internal class ColorArray private constructor(val arraySize: Int): Closable {
    private val arena = Arena()
    val lvColorArrayPtr: CArrayPointer<lv_color_t> = arena.allocArray(arraySize)

    fun replaceAll(color: Color) {
        (0 until arraySize).forEach { lvColorArrayPtr[it].full = color.full }
    }

    companion object {
        fun create(arraySize: Int): ColorArray = ColorArray(arraySize)
    }

    override fun close() {
        arena.clear()
    }
}

internal fun CValue<lvglkt_color_t>.toColor(): Color = Color.create().apply {
    var newFull = 255u
    var newRed = 255u.toUByte()
    var newGreen = 255u.toUByte()
    var newBlue = 255u.toUByte()
    this@toColor.useContents {
        newFull = full
        newRed = red
        newGreen = green
        newBlue = blue
    }
    full = newFull
    red = newRed
    green = newGreen
    blue = newBlue
}

internal fun colorFromRgb(red: UByte, green: UByte, blue: UByte): Color =
    lvglkt_color_make(red = red, green = green, blue = blue).toColor()
