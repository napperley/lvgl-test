package org.example.lvglTest

import lvgl.keyboard_read
import lvgl.mousewheel_read
import org.example.lvglTest.input.InputDevice
import org.example.lvglTest.input.InputDeviceDriver
import org.example.lvglTest.input.InputDeviceType

internal fun setupKeyboard(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.KEYPAD
    inDevDriver.onRead = { driver, data -> keyboard_read(driver.lvIndevDrvPtr, data.lvIndevDataPtr) }
    // Register the input device so it can be used.
    val keyboardInDev = inDevDriver.register()
    return inDevDriver to keyboardInDev
}

internal fun setupMouseWheel(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.ENCODER
    inDevDriver.onRead = { driver, data -> mousewheel_read(driver.lvIndevDrvPtr, data.lvIndevDataPtr) }
    // Register the input device so it can be used.
    val mouseWheelInDev = inDevDriver.register()
    return inDevDriver to mouseWheelInDev
}

internal fun setupTouchScreen(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.POINTER
    // Register the input device so it can be used.
    val touchScreenInDev = inDevDriver.register()
    return inDevDriver to touchScreenInDev
}
