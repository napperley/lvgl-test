package org.example.lvglTest.display

import kotlinx.cinterop.*
import lvgl.lv_color_t
import lvgl.lv_disp_draw_buf_init
import lvgl.lv_disp_draw_buf_t
import org.example.lvglTest.Closable

internal class DisplayDrawBuffer private constructor(
    ptr: CPointer<lv_disp_draw_buf_t>?,
    bufSize: UInt,
    dblBuffer: Boolean
): Closable {
    private val arena = Arena()
    private val primaryBufPtr: CPointer<lv_color_t>? =
        if (ptr == null) arena.allocArray(bufSize.toInt()) else null
    private val secondaryBufPtr: CPointer<lv_color_t>? =
        if (ptr == null && dblBuffer) arena.allocArray(bufSize.toInt()) else null
    val lvDispDrawBufPtr: CPointer<lv_disp_draw_buf_t> = ptr ?: createDrawBuffer(bufSize).ptr

    private fun createDrawBuffer(bufSize: UInt): lv_disp_draw_buf_t {
        val result = arena.alloc<lv_disp_draw_buf_t>()
        lv_disp_draw_buf_init(draw_buf = result.ptr, buf1 = primaryBufPtr, buf2 = secondaryBufPtr,
            size_in_px_cnt = bufSize)
        return result
    }

    companion object {
        fun create(bufSize: UInt, dblBuffer: Boolean): DisplayDrawBuffer =
            DisplayDrawBuffer(bufSize = bufSize, dblBuffer = dblBuffer, ptr = null)
    }

    override fun close() {
        if (primaryBufPtr != null) arena.clear()
    }
}
