package org.example.lvglTest.display

import kotlinx.cinterop.*
import lvgl.lv_disp_drv_init
import lvgl.lv_disp_drv_t
import lvgl.monitor_flush
import org.example.lvglTest.Area
import org.example.lvglTest.Closable
import org.example.lvglTest.Color
import org.example.lvglTest.toArea

internal class DisplayDriver private constructor(
    ptr: CPointer<lv_disp_drv_t>? = null,
    drawBuf: DisplayDrawBuffer? = null
) : Closable {
    private val arena = Arena()
    private val stableRef = if (ptr == null) StableRef.create(this) else null
    val lvDispDrvPtr: CPointer<lv_disp_drv_t>? = if (drawBuf != null) createDisplayDriver(drawBuf).ptr else ptr
    var horRes: Short
        get() = lvDispDrvPtr?.pointed?.hor_res ?: 0.toShort()
        set(value) {
            lvDispDrvPtr?.pointed?.hor_res = value
        }
    var vertRes: Short
        get() = lvDispDrvPtr?.pointed?.ver_res ?: 0.toShort()
        set(value) {
            lvDispDrvPtr?.pointed?.ver_res = value
        }
    var onFlush: (driver: DisplayDriver, area: Area, buf: Color) -> Unit = { _, _, _ -> }

    private fun createDisplayDriver(drawBuf: DisplayDrawBuffer): lv_disp_drv_t {
        val result = arena.alloc<lv_disp_drv_t>()
        lv_disp_drv_init(result.ptr)
        result.draw_buf = drawBuf.lvDispDrawBufPtr
        return result
    }

    private fun setupCallbacks() {
        lvDispDrvPtr?.pointed?.user_data = stableRef?.asCPointer()
        lvDispDrvPtr?.pointed?.flush_cb = staticCFunction { driver, area, buf ->
            val userData = driver?.pointed?.user_data?.asStableRef<DisplayDriver>()?.get()
            val color = Color.fromPointer(buf)
            userData?.onFlush?.invoke(driver.toDisplayDriver(), area.toArea(), color)
        }
    }

    override fun close() {
        arena.clear()
        stableRef?.dispose()
    }

    companion object {
        fun create(drawBuf: DisplayDrawBuffer): DisplayDriver {
            val result = DisplayDriver(drawBuf = drawBuf)
            result.setupCallbacks()
            return result
        }

        fun fromPointer(ptr: CPointer<lv_disp_drv_t>?): DisplayDriver = DisplayDriver(ptr = ptr)
    }
}

internal fun DisplayDriver.flush(area: Area, colorBuf: Color) {
    monitor_flush(area = area.lvAreaPtr, disp_drv = lvDispDrvPtr, color_p = colorBuf.lvColorPtr)
}

internal fun CPointer<lv_disp_drv_t>?.toDisplayDriver(): DisplayDriver = DisplayDriver.fromPointer(ptr = this)
