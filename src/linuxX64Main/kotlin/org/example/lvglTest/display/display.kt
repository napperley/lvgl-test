package org.example.lvglTest.display

import kotlinx.cinterop.CPointer
import lvgl.lv_disp_drv_register
import lvgl.lv_disp_t

internal class Display private constructor(ptr: CPointer<lv_disp_t>?) {
    val lvDispPtr: CPointer<lv_disp_t>? = ptr

    companion object {
        fun fromPointer(ptr: CPointer<lv_disp_t>?): Display = Display(ptr)
    }
}

internal fun CPointer<lv_disp_t>?.toDisplay(): Display = Display.fromPointer(this)

internal fun DisplayDriver.register(): Display = lv_disp_drv_register(lvDispDrvPtr).toDisplay()
