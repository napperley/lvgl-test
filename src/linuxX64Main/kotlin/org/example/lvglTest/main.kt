package org.example.lvglTest

import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.staticCFunction
import lvgl.*
import org.example.lvglTest.display.Display
import org.example.lvglTest.display.DisplayDrawBuffer
import org.example.lvglTest.display.DisplayDriver
import platform.posix.usleep

private var displayBuffer: DisplayDrawBuffer? = null
private var displayDriver: DisplayDriver? = null
private var display: Display? = null
private lateinit var msgLbl: CPointer<lv_obj_t>
private var counter = 0u

fun main() {
    println("Starting LVGL Test...")
    lv_init()
    val tmp = Sdl2.initHal(false)
    displayBuffer = tmp.first
    displayDriver = tmp.second
    display = tmp.third
    println("LVGL Version: $LVGL_VERSION_MAJOR.$LVGL_VERSION_MINOR.$LVGL_VERSION_PATCH")
    println("Display Resolution: ${displayDriver?.horRes}x${displayDriver?.vertRes}")
    setupUI()
    runEventLoop()
}

private fun runEventLoop() {
    // Handle LVGL tasks (tickless mode).
    while (true) {
        lv_tick_inc(5)
        lv_task_handler()
        usleep(5000)
    }
}

private fun onButtonClicked(evt: CPointer<lv_event_t>?) {
    val code = evt?.pointed?.code
    if (code == LV_EVENT_CLICKED) updateMsgLbl()
}

private fun updateMsgLbl() {
    counter++
    lv_label_set_text(msgLbl, "Button clicked $counter times")
}

private fun createMainLayout(): CPointer<lv_obj_t>? {
    val result = lv_obj_create(lv_scr_act())
    lv_obj_set_flex_flow(result, LV_FLEX_FLOW_COLUMN)
    lv_obj_set_size(obj = result, w = lv_pct(100), h = LV_SIZE_CONTENT.toShort())
    return result
}

private fun createBtn(mainLayout: CPointer<lv_obj_t>?): CPointer<lv_obj_t>? {
    val btn = lv_btn_create(mainLayout)
    val btnLbl = lv_label_create(btn)
    lv_label_set_text(btnLbl, "Click Me!")
    lv_obj_add_event_cb(
        obj = btn, user_data = null, filter = LV_EVENT_ALL,
        event_cb = staticCFunction(::onButtonClicked)
    )
    return btn
}

private fun createMsgLbl(mainLayout: CPointer<lv_obj_t>?): CPointer<lv_obj_t>? {
    val result = lv_label_create(mainLayout)
    // Must provide text that isn't empty otherwise a seg fault will occur!
    lv_label_set_text(result, " ")
    return result
}

private fun setupUI() {
    val mainLayout = createMainLayout()
    createBtn(mainLayout)
    msgLbl = createMsgLbl(mainLayout)!!
}
