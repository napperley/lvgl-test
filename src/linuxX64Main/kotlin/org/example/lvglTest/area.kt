package org.example.lvglTest

import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.ptr
import lvgl.*

internal class Area private constructor(ptr: CPointer<lv_area_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    val lvAreaPtr: CPointer<lv_area_t>? = ptr ?: arena?.alloc<lv_area_t>()?.ptr

    companion object {
        fun fromPointer(ptr: CPointer<lv_area_t>?): Area = Area(ptr)

        fun create(): Area = Area()
    }

    fun setPositionAndSize(left: Short, top: Short, right: Short, bottom: Short) {
        lv_area_set(area_p = lvAreaPtr, x1 = left, x2 = right, y1 = top, y2 = bottom)
    }

    override fun close() {
        arena?.clear()
    }
}

internal fun CPointer<lv_area_t>?.toArea(): Area = Area.fromPointer(this)
