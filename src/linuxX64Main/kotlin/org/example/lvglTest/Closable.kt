package org.example.lvglTest

internal interface Closable {
    fun close()
}
